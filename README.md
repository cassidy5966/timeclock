# HiMama Coding Challenge

## How did you approach this challenge?
I started by mapping out rough user flows and the associated database models. This project is pretty simple, so I had one diagram of a user logging in and landing on a page showing their time clock logs as well as a button that allows them to start and stop the clock, and a tiny ERD showing the two database models.

I then spun up a new Rails project and added a few libraries in that I knew I'd want to use. I chose Devise for authentication because it takes care of all of the details out of the box. I also chose to use rails scaffolds initially so I could create the files I needed quickly.

After tidying up some extra files from the scaffolding, I wrote functional tests for the TimeLogsController. As I did this, there were some changes I had to make to the controller to make it conform to my tests.

Early on, I considered using a controller dedicated to toggling the time clock off, however I didn't implement this until later. I chose to add this controller, because it makes the functionality of the controllers more clear and prevents the use of conditionals based on user input.

I did take a short break from this approach in the middle to do a bit of a refactor by adding in a presenter for the TimeLog and by making the UI a little more friendly.

## What schema design did you choose and why?
I chose to create an entry in a time_logs table with a start time and an end time every time the user clocks in or out. This means that users have many time logs, and one time log indicates a login and log out event. After working on this, I realize this resource should have been called "timer" as this would have made the concepts of "starting" and "stopping" the timer a little more clear.

This design allows for the concept of a "current_time_log" if the timer is running on one of the entries. Which translates nicely to the button that toggles between "Start Timer" and "Stop Timer".

## If you were given another day to work on this, how would you spend it?
* Improve styling on front end. I'd probably start with a baseline library like Foundation to kick start this.
* Make the site mobile-friendly, especially the table on the index page.
* A JS library to give me a starting point for a better date time picker.
* Pagination on time_logs#index
* I'd use Turbo Links to speed up some parts of the UI. I know that many people dislike Turbo Links, but because it's built into Rails, it is the quickest way to improve your user experience on a small app like this.

## What if you were given a month?
* Add an audit trail to time logs so we can see who edited them and when.
* Add a concept of an admin user who can see other users' time entries.
* Add a reporting feature for individual users and for admin users.
* Depending on the complexity of the user interface, I'd consider adding a JS front end like React.
