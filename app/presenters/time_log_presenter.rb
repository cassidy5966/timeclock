require 'time_difference'

class TimeLogPresenter < SimpleDelegator
  def date
    self[:start_time]&.to_date
  end

  def start_time
    self[:start_time]&.to_s(:time)
  end

  def end_time
    self[:end_time]&.to_s(:time)
  end

  def duration
    return if running?
    d = TimeDifference.between(self[:start_time], self[:end_time]).in_hours
    "#{d} hours"
  end
end
