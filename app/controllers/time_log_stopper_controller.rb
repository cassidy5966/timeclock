class TimeLogStopperController < ApplicationController
  before_action :set_time_log

  def update
    if @time_log.end_time.nil?
      @time_log.update(end_time: Time.now)
      redirect_to time_logs_path, notice: 'The timer has been stopped.'
    else
      redirect_to time_logs_path,
                  alert: "Could not end the timer."
    end
  end

  private
    def set_time_log
      @time_log = current_user.time_logs.find(params[:id])
    end
end
