class TimeLogsController < ApplicationController
  before_action :set_time_log, only: [:show, :edit, :update, :destroy]

  def index
    @time_logs = current_user.time_logs.map { |l| TimeLogPresenter.new(l) }
  end

  def create
    @time_log = current_user.time_logs.new(start_time: DateTime.now)

    if @time_log.save
      redirect_to time_logs_path, notice: 'Your timer was started.'
    else
      render :index, alert: 'There was an error starting your timer.'
    end
  end

  def update
    if @time_log.update(time_log_params)
      redirect_to time_logs_path, notice: 'Time log was successfully updated.'
    else
      render :edit
    end
  end

  def destroy
    @time_log.destroy
    redirect_to time_logs_url, notice: 'Time log was successfully destroyed.'
  end

  private
    def set_time_log
      @time_log = current_user.time_logs.find(params[:id])
    end

    def time_log_params
      params.require(:time_log).permit(:start_time, :end_time, :user_id)
    end
end
