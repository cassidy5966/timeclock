require 'time_difference'

module TimeLogsHelper
  def time_log_button
    current_log&.running? ? stop_timer : start_timer
  end

  def stop_timer
    link_to 'Stop Timer',
            time_log_stopper_path(current_log),
            method: :patch,
            class: :button

  end

  def start_timer
    link_to 'Start Timer', time_logs_path, method: :post, class: :button
  end

  private

  def current_log
    current_user.time_logs.last
  end
end
