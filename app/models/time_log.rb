class TimeLog < ApplicationRecord
  belongs_to :user

  validate :correct_duration, on: :update

  def running?
    end_time.nil?
  end

  private

  def correct_duration
    return true if end_time.nil?

    if start_time > end_time
      errors.add(:end_time, 'End time must be after start time')
    end
  end
end
