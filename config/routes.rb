Rails.application.routes.draw do
  devise_for :users
  resources :time_logs
  resources :time_log_stopper, only: [:update]
  root to: 'time_logs#index'
end
