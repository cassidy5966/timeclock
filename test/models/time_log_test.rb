require 'test_helper'

class TimeLogTest < ActiveSupport::TestCase
  setup do
    @time_log = time_logs(:one)
  end

  context 'associations' do
    should belong_to :user
  end

  context 'validations' do
    context 'when end_time is before start_time' do
      setup do
        @time_log.update(end_time: 1.year.ago)
      end

      should 'not be valid' do
        refute @time_log.valid?
      end

      should 'add an error to the time_log' do
        assert_equal @time_log.errors.messages.dig(:end_time, 0),
                     'End time must be after start time'
      end
    end
  end

  context '#running?' do
    context 'a log is running' do
      should 'return true' do
        @time_log.update(end_time: nil)
        assert @time_log.running?
      end
    end

    context 'a log has ended' do
      should 'return false' do
        @time_log.update(end_time: Time.now)
        refute @time_log.running?
      end
    end
  end
end
