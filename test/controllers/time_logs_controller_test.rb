require 'test_helper'

class TimeLogsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do 
    @user = users(:one)
    @time_log = @user.time_logs.first
    sign_in @user
  end

  context '#index' do
    context 'a user with time logs' do
      should 'return success' do
        get time_logs_url
        assert_response :success
      end
    end

    context 'a user with no time logs' do
      should 'return success' do
        @user.time_logs.destroy_all
        get time_logs_url
        assert_response :success
      end
    end
  end

  context '#create' do
    should 'create a new time log' do
      assert_difference 'users(:one).time_logs.count', 1 do
        post time_logs_path
      end
    end

    should 'add a start time to the time log' do
      post time_logs_path
      refute_nil @user.time_logs.last.start_time
    end

    should 'respond with redirect' do
      post time_logs_path
      assert_response :redirect
    end
  end

  context '#update' do
    setup do 
      @end_time = Time.zone.now
    end

    should 'update the time log' do
      assert_changes '@time_log.reload.end_time' do
        put time_log_path id: @time_log.id, time_log: { end_time: @end_time }
      end
    end

    context 'with a log that belongs to another user' do
      setup do
        @time_log = users(:two).time_logs.first
      end

      should 'raise an error' do
        assert_raises ActiveRecord::RecordNotFound do
          put time_log_path id: @time_log.id, time_log: { end_time: @end_time }
        end
      end
    end
  end

  context '#destroy' do
    should 'delete a time log' do
      assert_difference '@user.time_logs.count', -1 do
        delete time_log_path id: @time_log.id
      end
    end

    context 'with a log that belongs to another user' do
      setup do
        @time_log = users(:two).time_logs.first
      end

      should 'raise an error' do
        assert_raises ActiveRecord::RecordNotFound do
          delete time_log_path id: @time_log.id
        end
      end
    end
  end
end
