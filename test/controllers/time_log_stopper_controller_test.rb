require 'test_helper'

class TimeLogStopperControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  setup do
    @time_log = time_logs(:one)
    user = @time_log.user
    sign_in user
  end

  context '#update' do
    context 'with a running log' do
      setup do
        @time_log.update(start_time: Time.now, end_time: nil)
      end

      should 'redirect do time_logs#index' do
        patch time_log_stopper_path id: @time_log.id
        assert_redirected_to time_logs_path
      end

      should 'set and end time' do
        assert_changes '@time_log.reload.end_time' do
          patch time_log_stopper_path id: @time_log.id
        end
      end
    end

    context 'with an ended log' do
      setup do
        @time_log.update(start_time: Time.now, end_time: 1.hour.from_now)
      end

      should 'redirect to time_logs#index' do
        patch time_log_stopper_path id: @time_log.id
        assert_redirected_to time_logs_path
      end

      should 'not set and end time' do
        assert_no_changes '@time_log.reload.end_time' do
          patch time_log_stopper_path id: @time_log.id
        end
      end
    end

    context 'with a log that belongs to another user' do
      setup do
        @time_log = users(:two).time_logs.first
      end

      should 'raise an error' do
        assert_raises ActiveRecord::RecordNotFound do
          patch time_log_stopper_path id: @time_log.id
        end
      end
    end
  end
end
